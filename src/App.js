import React from "react";
import { BrowserRouter } from "react-router-dom";

// redux
import { Provider } from "react-redux";
import { store, persistor } from "./redux/store";
import { PersistGate } from "redux-persist/integration/react";

// firebase-redux
import { ReactReduxFirebaseProvider } from "react-redux-firebase";
import { createFirestoreInstance } from "redux-firestore";
import firebase, { rrfConfig } from "./config/firebase";
// routes
import MainRoute from "./routes/main.route";

// ReactReduxFirebaseProvider props
const rrfProps = {
	firebase,
	config: rrfConfig,
	dispatch: store.dispatch,
	createFirestoreInstance,
};

const App = () => {
	return (
		<Provider store={store}>
			<ReactReduxFirebaseProvider {...rrfProps}>
				<PersistGate loading={null} persistor={persistor}>
					<BrowserRouter>
						<MainRoute />
					</BrowserRouter>
				</PersistGate>
			</ReactReduxFirebaseProvider>
		</Provider>
	);
};

export default App;
