import React from "react";
import styled from "styled-components";
import { Text } from "../styles/typography.styles";
import THEME from "../styles/color.styles";

const Container = styled.div`
	display: flex;
	height: 80px;
	width: 100%;
	justify-content: center;
	align-items: center;
	border: 1px solid ${THEME.red};
`;

const Header = () => {
	console.log("Header render");
	return (
		<Container>
			<Text>I'm Header</Text>
		</Container>
	);
};

export default Header;
