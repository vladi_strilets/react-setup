import React from "react";
import styled from "styled-components";
import { Text } from "../styles/typography.styles";
import THEME from "../styles/color.styles";

const Container = styled.div`
	display: flex;
	height: 160px;
	width: 100%;
	justify-content: center;
	align-items: center;
	border: 1px solid ${THEME.red};
`;

const Footer = () => {
	console.log("Footer render");
	return (
		<Container>
			<Text>I'm Footer</Text>
		</Container>
	);
};

export default Footer;
