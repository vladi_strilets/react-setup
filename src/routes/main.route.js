import React, { Fragment } from "react";
import { Switch, Route, useRouteMatch } from "react-router-dom";

// pages
import HomePage from "../pages/homepage.page";
import DashboardPage from "../pages/dashboard.page";
import UnknownPage from "../pages/unknownpage.page";

// comun pages containers
import Header from "../containers/header.container";
import Footer from "../containers/footer.container";

// routes types
import ROUTES from "./routes.types.js";

const MainRoute = () => {
	return (
		<Fragment>
			<Header />
			<Switch>
				<Route exact path={ROUTES.HOME} component={HomePage} />
				<Route exact path={ROUTES.PANEL} component={DashboardPage} />
				<Route path='/' component={UnknownPage} />
			</Switch>
			<Footer />
		</Fragment>
	);
};

export default MainRoute;
