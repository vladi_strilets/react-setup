import styled from "styled-components";

import THEME from "./color.styles";

export const Display = styled.h1`
	font-size: 48px;
	font-weight: 600;
	color: ${(props) => (props.color ? props.color : THEME.text_color)};
`;

export const Text = styled.p`
	font-size: 16px;
	font-weight: 500;
	color: ${(props) => (props.color ? props.color : THEME.text_color)};
`;
