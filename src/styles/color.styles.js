const THEME = {
	blue: "#2200CB",
	green: "#3AA1AB",
	yellow: "#E2B03D",
	red: "#E5386D",
	light_white: "#F8F0EE",
	text_color: "#222222",
	white: "#FFFFFF",
	black: "#000000",
};

export default THEME;
