const INITIAL_STATE = {};

const userReducer = (state = INITIAL_STATE, action) => {
	switch (action.key) {
		case "SOME_ACTION":
			return { ...state };

		default:
			return state;
	}
};

export default userReducer;
