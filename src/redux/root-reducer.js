import { combineReducers } from "redux";
import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

// reducers
import { firebaseReducer } from "react-redux-firebase";
import { firestoreReducer } from "redux-firestore";

// personal reducers
import userReducer from "./user/user.reducer";
import hardSet from "redux-persist/es/stateReconciler/hardSet";

const persistConfig = {
	key: "root",
	storage,
	stateReconciler: hardSet,
	whitelist: ["user"],
};

const rootReducer = combineReducers({
	firebase: firebaseReducer,
	firestore: firestoreReducer,
	user: userReducer,
});

// export default rootReducer;

export default persistReducer(persistConfig, rootReducer);
