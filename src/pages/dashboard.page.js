import React from "react";
import styled from "styled-components";
import THEME from "../styles/color.styles";
import { Text, Display } from "../styles/typography.styles";
import { Link } from "react-router-dom";

const Container = styled.div`
	display: flex;
	height: 500px;
	width: 100%;
	justify-content: center;
	align-items: center;
	border: 1px solid ${THEME.red};
	flex-direction: column;
`;

const DashboardPage = () => {
	console.log("DashboardPage render");
	return (
		<Container>
			<Display>I'm Dashboard Page</Display>
			<Link to='/'>
				<Text>Go to HomePage</Text>
			</Link>
		</Container>
	);
};

export default DashboardPage;
