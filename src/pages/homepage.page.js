import React from "react";
import styled from "styled-components";
import THEME from "../styles/color.styles";
import { Text, Display } from "../styles/typography.styles";
import { Link } from "react-router-dom";

const Container = styled.div`
	display: flex;
	height: 500px;
	width: 100%;
	justify-content: center;
	align-items: center;
	border: 1px solid ${THEME.red};
	flex-direction: column;
`;

const HomePage = () => {
	console.log("HomePage render");
	return (
		<Container>
			<Display>I'm Homepage</Display>
		</Container>
	);
};

export default HomePage;
