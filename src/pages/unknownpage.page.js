import React from "react";
import styled from "styled-components";
import THEME from "../styles/color.styles";
import { Text, Display } from "../styles/typography.styles";
import { Link } from "react-router-dom";

const Container = styled.div`
	display: flex;
	height: 500px;
	width: 100%;
	justify-content: center;
	align-items: center;
	border: 1px solid ${THEME.red};
	flex-direction: column;
`;

const UnknownPage = () => {
	console.log("UnknownPage render");
	return (
		<Container>
			<Display>Ups, página no existe</Display>
			<Link to='/'>
				<Text>Go to HomePage</Text>
			</Link>
		</Container>
	);
};

export default UnknownPage;
