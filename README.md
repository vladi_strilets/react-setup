## React Setup

Useful setups for React projects, just clone and use.

TODO: better explication of branch cloning

Current versions:

- React + Redux + Router v0.1

```
// redux
npm i redux react-redux redux-persist redux-logger redux-thunk reselect

// router
npm i react-router-dom

// styled-components
npm i styled-components
```

- React + Redux + Router + Firebase v0.1

```
// all from React Redux Router
// firebase
npm i firebase react-redux-firebase react-firestore
```

## Notes

Firebase config throught .env. Also, React DevTools are installed. Master brach
includes all of stuff installed.
